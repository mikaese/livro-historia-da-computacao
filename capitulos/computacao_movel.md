# Computação Movel

A computação movel tem como principal objetivo entregar acesso à informação ao usuário em qualquer lugar, a qualquer momento e com qualquer equipamento.

Graças ao avanço na tecnologia, a expansão e evolução dos smartphones e em redes de internet, hoje estamos conectados o tempo todo em todo lugar com um aparelho que cabe em nosso bolso, e continua evoluindo a cada dia.
