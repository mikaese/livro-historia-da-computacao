# Bibliografia


1. ANTÔNIO HOUAISS (Rio de Janeiro). Instituto Antônio Houaiss de Lexicografia (ed.). Computador. In: ANTÔNIO HOUAISS. Instituto Antônio Houaiss de Lexicografia (ed.). Minidicionário Houaiss da língua portuguesa. 3. ed. Rio de Janeiro: Objetiva, 2009. p. 175.

1. PORTAL SÃO FRANCISCO. Portal São Francisco. Computador Analógico. Disponível em: https://www.portalsaofrancisco.com.br/curiosidades/computador-analogico#:~:text=A%20segunda%20diferen%C3%A7a%20%C3%A9%20que,digital%20funciona%20com%20n%C3%BAmeros%20discretos.&text=Os%20computadores%20mec%C3%A2nicos%20existiram%20h%C3%A1,conhecido%20mais%20antigo%20o%20Antikythera.. Acesso em: 21 abr. 2021.

1. KISTERMANN, F. W.. The Invention and Development of the Hollerith Punched Card: in commemoration of the 130th anniversary of the birth of herman hollerith and for the 100th anniversary of large scale data processing. Ieee Annals Of The History Of Computing, [S.L.], v. 13, n. 3, p. 245-259, jul. 1991. Institute of Electrical and Electronics Engineers (IEEE). http://dx.doi.org/10.1109/mahc.1991.10023.

1. GADELHA, Julia. A evolução dos computadores. Disponível em: http://www.ic.uff.br/~aconci/evolucao.html#:~:text=O%20primeiro%20computador%20pessoal%20%C3%A9,pela%20empresa%20de%20softwares%20Microsoft.. Acesso em: 21 abr. 2021.

1. FUNDAÇÃO BRADESCO. Microinformática. Resumo da evolução dos computadores. Disponível em: http://www.fundacaobradesco.org.br/vv-apostilas/mic_pag3.htm.. Acesso em: 22 abr. 2021.

1. MAESTROVIRTUALE. Maestrovirtuale. O que é a quinta geração de computadores?. Disponível em: https://maestrovirtuale.com/o-que-e-a-quinta-geracao-de-computadores/#:~:text=Este%20projeto%20teve%20como%20objetivo,implementar%20hardware%20e%20software%20avan%C3%A7ados.. Acesso em: 22 abr. 2021.

1. LOGICAL MINDS. Logical Minds. O que é computação móvel e como você pode fazer uso desta tecnologia?. Disponível em: http://logicalminds.com.br/o-que-e-computacao-movel-e-como-voce-pode-fazer-uso-dessa-tecnologia/#:~:text=A%20computa%C3%A7%C3%A3o%20m%C3%B3vel%20%C3%A9%20um,impercept%C3%ADvel%20e%20percept%C3%ADvel%20ao%20usu%C3%A1rio.. Acesso em: 22 abr. 2021.
