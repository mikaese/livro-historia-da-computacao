# Evolução dos computadores pessoais
O computador pessoal passou por muitas mudanças para chegar nos computadores que conhecemos hoje em dia, mudanças que são marcadas por cinco grandes gerações

## Primeira Geração:
Ocorre entre o período de 1951 a 1959, funcionavam a partir de circuitos eletrônicos e válvulas , faziam cálculos em milésimos de segundo e eram programados em linguagem de máquina.

## Segunda Geração:
Ocorre entre 1959 e 1965, as válvulas foram trocadas por transistores, o que reduziu substancialmente consumo de energia e o seu tamanho, tinham capacidade de calcular em microssegundos e eram programados em linguagem montadora.

## Terceira Geração:
Compreende os computadores construídos entre 1965 e 1975, sua construção e funcionamento se baseia em circuitos integrados, o que diminuiu drasticamente seu tamnaho e aumentou sua capacidade de processamento, sendo capaz de realizar cálculos em nanossegundos, usando nessa época uma linguagem de programação de alto nível.

## Quarta Geração:
A partir de 1975 até os dias atuais, neste período o microprocessador foi integrado aos computadores e com o surgimento dos microcomputadores a informática ficou cada vez mais popular, tornando os microcomputadores parte da vida do ser humano.

## Quinta Geração:
Teve seu início por volta de 1980, a quinta geração é constituida pelos supercomputadores, tem como objetivo a criação de máquinas capazes de processar linguagem natural e atingir habilidades lógicas, resolvendo problemas complexos de codificação, abrangendo as áreas de inteligencia artificial, robótica, redes de comunicação, nanotecnologia, computação quântica, etc. 
