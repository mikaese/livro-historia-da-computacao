# Introdução 

 Desde nosso nascimento como espécie, o que nos tornava diferente de outros primatas era o uso de ferramentas no nosso cotidiano. Se iniciou com pedras e paus, depois o fogo e assim foi seguindo, quanto mais complexa era a tarefa, mais complexa era a ferramenta. O que mudou com o passar do tempo foi o nosso conhecimento e como usamos ele.

 Hoje em dia nosso foco principal está na integração do nosso dia a dia à computação. Aonde quer que estejamos, é muito fácil vermos algo que tenha um computador embracado, desde carros até portas. E é justamente nessa interação que a Engenharia da Computação nasceu e cresceu. 

 Tendo em vista o tema que iremos conversar, é importante explicarmos alguns termos.

 Um **computador** é um equipamento capaz de guardar, analisar e processar dados de acordo com programas previamente estabelecidos, em outras palavras, uma máquina feita para fazer cálculos.

 Ele pode ser mecânico, ou seja, que depende de peças e mecanismos físicos para fazer seu trabalho, ou eletrônico, utilizando peças e componentes elétricos para funcionar.

 Outra especificação é que o computador pode ser digital, que siginifica que ele trabalha somente com números racionais, visto que esses podem ser restritos a natureza binária dessa computação, no entanto são simples e velozes, o que faz com que sejam amplamente utilizados no dia a dia. Em contra partida existem os computadores analógicos, mais lentos que os digitais, porém extremamente precisos, e por conta disso, são utilizados principalmente na aréa científica e aeronáutica.

Ele ser analógico ou digital não limita ele se ser mecânico ou eletrônico.
