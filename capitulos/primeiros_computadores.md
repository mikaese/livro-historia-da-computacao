# Primeiros computadores

Em 1890, a pedido do United States Census Bureau, Herman Holerrith cria o considerado primeiro computador mecânico. A maquina lia cartões de papel perfurados em código BCD e efetuava contagens de informação referente à perfuração respectiva.
 
 ![](imagens/Cartão.png)

 A partir de 1930, começam as pesquisas para substituir as partes mecânicas por elétricas. O Mark I, concluído em 1944 por uma equipe liderada por Howard Aiken, é o primeiro computador eletromecânico capaz de efetuar cálculos mais complexos sem a interferência humana. Ele mede 15 m x 2,5 m e demora 11 segundos para executar um cálculo.

 ![](imagens/mark1.jpeg)

 Em 1946, surge o Eniac (Electronic Numerical Integrator and Computer), primeiro computador eletrônico e digital automático: pesa 30 toneladas, emprega cerca de 18 mil válvulas e realiza 4.500 cálculos por segundo. O Eniac contém a arquitetura básica de um computador, empregada até hoje: memória principal (área de trabalho), memória auxiliar (onde são armazenados os dados), unidade central de processamento (o "cérebro" da máquina, que executa todas as informações) e dispositivos de entrada e saída de dados que atualmente permitem a ligação de periféricos como monitor, teclado, mouse, scanner, tela, impressora, entre outros.

 ![](imagens/eniac.jpeg)




