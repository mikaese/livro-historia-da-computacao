# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Introdução](capitulos/introducao.md)
1. [Primeiros Computadores](capitulos/primeiros_computadores.md)
1. [Evolução dos Computadores Pessoais e sua Interconexão](capitulos/evolucao.md)
1. [Computação Móvel](capitulos/computacao_movel.md)
1. [Futuro](capitulos/futuro.md)
1. [Bibliografia](capitulos/bibliografia.md)



## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/8279871/avatar.png?width=400)  | Lucas Mikaese | mikaese | [lucas_phbsp@hotmail.com](mailto:lucas_phbsp@hotmail.com)
| ![](https://secure.gravatar.com/avatar/9bbdc5d80f81dec2d83e129df4bca6ef?s=180&d=identicon) | Marcel Assad | MarcelAssad | [marceldoelinger@gmail.com](mailto:marceldoelinger@gmail.com)
| ![](https://secure.gravatar.com/avatar/a9807befc282e75ea34696ed1369fe2c?s=200&d=identicon) | Samuel Novakoski | SamuelN13 | [samuelnovakoski@gmail.com](mailto:samuelnovakoski@gmail.com)
| ![](https://secure.gravatar.com/avatar/b61b2c2cfbecd42532363c1ee65026d1?s=200&d=identicon) | Jude Mathieu | @judematieu | [@gmail.com] (mailto:@gmail.com)
